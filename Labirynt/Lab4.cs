﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin;

namespace Collin_Game.Labirynt
{
    class Lab4 : DrawableGameComponent
    {
        // private MainGame mainGame;
        private Game game;

        Texture2D Player;
        Vector2 SpritePos;

        Texture2D Robot;
        Vector2 RobotPos;
        Rectangle recRobot;

        Texture2D lmap2;
        Vector2 poslmap2;
        Rectangle reclmap2;

        public bool walkRobot = false;

        Texture2D kluczyk;
        Vector2 pos_key;
        Rectangle rec_key;


        Texture2D healthBar;
        Vector2 pos_healthBar;
        Rectangle rec_healthBar;

        public Lab4(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }

        public override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            //create new objects

        }
        protected override void LoadContent()
        {
            Player = game.Content.Load<Texture2D>("collin2");
            

            Robot = game.Content.Load<Texture2D>("robot");
            RobotPos = new Vector2(300, 300);

            lmap2 = game.Content.Load<Texture2D>("lab4");

            kluczyk = game.Content.Load<Texture2D>("key");
            //  rec_key = new Rectangle(0, 0, 0, 0);

            healthBar = game.Content.Load<Texture2D>("Pasek_zycia");

            pos_healthBar = new Vector2(10, 450);
            // healthBar.Width = 150
            // healthBar.Height = 20
            rec_healthBar = new Rectangle(0, 0, 150, 15);

            SpritePos = new Vector2(10, 10);

        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);
            spriteBatch.Begin();

            spriteBatch.Draw(lmap2, new Vector2(0, 0), new Rectangle(0, 0, 600, 500), Color.White);
            spriteBatch.Draw(Robot, RobotPos, Color.White);
            spriteBatch.Draw(Player, SpritePos, Color.White);

            spriteBatch.Draw(healthBar, pos_healthBar, rec_healthBar, Color.White);
            spriteBatch.End();

        }
        private void MoveSprite()
        {
            KeyboardState currKey = Keyboard.GetState();
            if (currKey.IsKeyDown(Keys.Left)) SpritePos.X -= 2;
            if (currKey.IsKeyDown(Keys.Right)) SpritePos.X += 2;
            if (currKey.IsKeyDown(Keys.Up)) SpritePos.Y -= 2;
            if (currKey.IsKeyDown(Keys.Down)) SpritePos.Y += 2;

        }
        private void Health()
        {
                if (rec_healthBar.Width >= 150) rec_healthBar.Width = 150;

                if (rec_healthBar.Width <= 0)
                {
                    rec_healthBar.Width = 0;

                    if (rec_healthBar.Width == 0)
                    {
                        MenuState.IsShowGameOverScene = true;
                        rec_healthBar.Width = 150;
                        SpritePos = new Vector2(50, 50);
                    }

                }

            }


        public void Update()
        {
            MoveSprite();
            Health();



            //PRZEJSCIA DO INNYCH MAP

            if (SpritePos.Y <= 0) MenuState.IsShowLab3 = true;
         
            if (SpritePos.X>= 540) MenuState.Forest_1 = true;


            if (SpritePos.X <= 60 && SpritePos.X>= 55 && SpritePos.Y >= 55 && SpritePos.Y <= 460) SpritePos.X = 60;
            if ( SpritePos.Y >= 460) SpritePos.Y = 460;
            if (SpritePos.Y <= 100 && SpritePos.X > 50 && SpritePos.X < 150) SpritePos.Y = 100;
            if (SpritePos.X <= 155 && SpritePos.X > 140 && SpritePos.Y < 100) SpritePos.X = 155;
            if(SpritePos.X >= 230 && SpritePos.X <= 220 && SpritePos.Y < 200) SpritePos.X = 230;
            if (SpritePos.X <= 265 && SpritePos.X >= 245 && SpritePos.Y < 200) SpritePos.X = 265;
            if (SpritePos.X > 260 && SpritePos.Y < 10) SpritePos.Y = 10;
            if (SpritePos.Y > 0 && SpritePos.X > 535 && (SpritePos.Y <255 || SpritePos.Y > 355) ) SpritePos.X = 535;

            if (SpritePos.Y > 240 && SpritePos.Y < 260 && SpritePos.X < 545 && SpritePos.X > 400) SpritePos.Y = 240;
            if (SpritePos.Y < 290 && SpritePos.Y > 265 &&  SpritePos.X > 400) SpritePos.Y = 290;

            if (SpritePos.Y > 320 && SpritePos.Y < 325 && SpritePos.X > 400) SpritePos.Y = 320;
            if (SpritePos.Y < 370 && SpritePos.Y > 365 && SpritePos.X > 400 && SpritePos.X < 560) SpritePos.Y = 370;


          
       




        }
    }
}
