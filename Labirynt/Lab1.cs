﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin;

namespace Collin_Game.Labirynt
{
    class Lab1 : DrawableGameComponent
    {
        // private MainGame mainGame;
        private Game game;
        bool walkCollin = true;

        Texture2D Player;
        Vector2 SpritePos;

        Texture2D Robot;
        Vector2 RobotPos;
        Rectangle recRobot;

        Texture2D lmap1;
        Vector2 poslmap1;
        Rectangle reclmap1;

        public bool walkRobot = false;

        Texture2D kluczyk;
        Vector2 pos_key;
        Rectangle rec_key;


        Texture2D healthBar;
        Vector2 pos_healthBar;
        Rectangle rec_healthBar;

        public Lab1(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }

        public override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            //create new objects

        }
        protected override void LoadContent()
        {
            Player = game.Content.Load<Texture2D>("collin2");
             SpritePos = new Vector2(0, 300);

            Robot = game.Content.Load<Texture2D>("robot");
            RobotPos = new Vector2(300, 300);

            lmap1 = game.Content.Load<Texture2D>("lab1");

            kluczyk = game.Content.Load<Texture2D>("key");
          //  rec_key = new Rectangle(0, 0, 0, 0);

           healthBar = game.Content.Load<Texture2D>("Pasek_zycia");

            pos_healthBar = new Vector2(10, 450);
            // healthBar.Width = 150
            // healthBar.Height = 20
            rec_healthBar = new Rectangle(0, 0, 150, 15);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            spriteBatch.Draw(lmap1, new Vector2(0,0), new Rectangle(0,0,600,500), Color.White);
            spriteBatch.Draw(Robot, RobotPos, Color.White);
            spriteBatch.Draw(Player, SpritePos, Color.White);

            spriteBatch.Draw(healthBar, pos_healthBar, rec_healthBar, Color.White);
            spriteBatch.End();

        }
        private void MoveSprite()
        {
            KeyboardState currKey = Keyboard.GetState();
            if (currKey.IsKeyDown(Keys.Left)) SpritePos.X -= 2;
            if (currKey.IsKeyDown(Keys.Right)) SpritePos.X += 2;
            if (currKey.IsKeyDown(Keys.Up)) SpritePos.Y -= 2;
            if (currKey.IsKeyDown(Keys.Down)) SpritePos.Y += 2;
        }
        private void Health()
        {
            
                if (rec_healthBar.Width >= 150)
                {
                    rec_healthBar.Width = 150;
                }

                if (rec_healthBar.Width <= 0)
                {
                    rec_healthBar.Width = 0;

                    if (rec_healthBar.Width == 0)
                    {
                        MenuState.IsShowGameOverScene = true;
                        rec_healthBar.Width = 150;
                        SpritePos = new Vector2(50, 50);
                    }

                
            }
        }
       private void  LimitsWalls()
        {
            if (SpritePos.Y <= 0) MenuState.IsShowLab2 = true;
            // if (SpritePos.X <= 0) MenuState.IsShowLevel_8 = true;
            if (SpritePos.X >= 550) SpritePos.X = 550;
            if (SpritePos.Y >= 450) SpritePos.Y = 450;
            if (SpritePos.X <= 0) SpritePos.X = 0;

            

            if (SpritePos.Y >= 385 && SpritePos.Y < 400 && SpritePos.X < 450) SpritePos.Y = 385;
         //   if ( SpritePos.Y <= 440 && SpritePos.Y > 390  && SpritePos.X <= 450) SpritePos.Y = 440;
         if (SpritePos.X <= 455 && SpritePos.Y > 385 && SpritePos.Y <= 450) SpritePos.X = 455;

           if(SpritePos.Y <= 260 && SpritePos.Y >= 250 && SpritePos.X < 470 ) SpritePos.Y = 260;
          if (SpritePos.Y >= 210 && SpritePos.Y < 260 && SpritePos.X < 470) SpritePos.Y = 210;
            if (SpritePos.X >= 430 && SpritePos.X<= 469&& SpritePos.Y > 0 && SpritePos.Y < 260) SpritePos.X = 430;

            if (SpritePos.X <= 475 && SpritePos.X>= 471 &&  SpritePos.Y>0   && SpritePos.Y <260) SpritePos.X = 475;

          
           
        }
        public void Update()
        {

       
            Health();
            LimitsWalls();
            if( walkCollin == true)
            {
                     MoveSprite();
            }
           
                if (walkRobot == false)
                {RobotPos.X += 1; }
                

                if(RobotPos.X >= 400)
                {
                    walkRobot = true;
                    RobotPos.X = 400;
                    }

              if(walkRobot == true)
            {
                RobotPos.X -= 1;
            }
            if (RobotPos.X <= 200)
            {
                walkRobot = false;
                RobotPos.X = 200;
            }
            if (SpritePos.X == RobotPos.X && SpritePos.Y == RobotPos.Y) rec_healthBar.Width -= 10;
            if (SpritePos.X <= RobotPos.X + 50 && SpritePos.X >= RobotPos.X - 20 && SpritePos.Y <= RobotPos.Y + 50 && SpritePos.Y >= RobotPos.Y - 30) rec_healthBar.Width -= 10;
        }
    }
}
