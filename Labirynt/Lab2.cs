﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin;

namespace Collin_Game.Labirynt
{
    class Lab2 : DrawableGameComponent
    {
        // private MainGame mainGame;
        private Game game;

        Texture2D Player;
        Vector2 SpritePos;

        Texture2D Robot;
        Vector2 RobotPos;
        Rectangle recRobot;

        Texture2D lmap2;
        Vector2 poslmap2;
        Rectangle reclmap2;

        public bool walkRobot = false;

        Texture2D kluczyk;
        Vector2 pos_key;
        Rectangle rec_key;


        Texture2D healthBar;
        Vector2 pos_healthBar;
        Rectangle rec_healthBar;

        public Lab2(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }

        public override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            //create new objects

        }
        protected override void LoadContent()
        {
            Player = game.Content.Load<Texture2D>("collin2");
         

            Robot = game.Content.Load<Texture2D>("robot");
            RobotPos = new Vector2(50, 100);

            lmap2 = game.Content.Load<Texture2D>("lab2");

            kluczyk = game.Content.Load<Texture2D>("key");
            //  rec_key = new Rectangle(0, 0, 0, 0);

            healthBar = game.Content.Load<Texture2D>("Pasek_zycia");

            pos_healthBar = new Vector2(10, 450);
            // healthBar.Width = 150
            // healthBar.Height = 20
            rec_healthBar = new Rectangle(0, 0, 150, 15);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            spriteBatch.Draw(lmap2, new Vector2(0, 0), new Rectangle(0, 0, 600, 500), Color.White);
            spriteBatch.Draw(Robot, RobotPos, Color.White);
            spriteBatch.Draw(Player, SpritePos, Color.White);

            spriteBatch.Draw(healthBar, pos_healthBar, rec_healthBar, Color.White);
            spriteBatch.End();

        }

        private void MoveSprite()
        {
            KeyboardState currKey = Keyboard.GetState();
            if (currKey.IsKeyDown(Keys.Left)) SpritePos.X -= 2;
            if (currKey.IsKeyDown(Keys.Right)) SpritePos.X += 2;
            if (currKey.IsKeyDown(Keys.Up)) SpritePos.Y -= 2;
            if (currKey.IsKeyDown(Keys.Down)) SpritePos.Y += 2;
        }
        private void HealthBar()
        {


            if (rec_healthBar.Width >= 150)
            {
                rec_healthBar.Width = 150;
            }

            if (rec_healthBar.Width <= 0)
            {
                rec_healthBar.Width = 0;

                if (rec_healthBar.Width == 0)
                {
                    MenuState.IsShowGameOverScene = true;
                    rec_healthBar.Width = 150;
                    SpritePos = new Vector2(50, 50);
                }


            }
        }
        private void ROBOT()
        {
            if (walkRobot == false)
            { RobotPos.X += 1; }


            if (RobotPos.X >=200)
            {
                walkRobot = true;
                RobotPos.X = 200;
            }

            if (walkRobot == true)
            {
                RobotPos.X -= 1;
            }
            if (RobotPos.X <= 50)
            {
                walkRobot = false;
                RobotPos.X = 50;
            }

            if (SpritePos.X == RobotPos.X && SpritePos.Y == RobotPos.Y) rec_healthBar.Width -= 10;
            if (SpritePos.X <= RobotPos.X + 50 && SpritePos.X >= RobotPos.X - 20 && SpritePos.Y <= RobotPos.Y + 50 && SpritePos.Y >= RobotPos.Y - 30) rec_healthBar.Width -= 6;
        }
        private void Walls()
        {
            if (SpritePos.X <= 0) SpritePos.X = 0;
            if (SpritePos.Y <= 0) SpritePos.Y = 0;
            if (SpritePos.X >= 530 && SpritePos.Y >= 80 && SpritePos.Y <= 200) MenuState.IsShowLab3 = true;
            if (SpritePos.X >= 530)/* && SpritePos.Y <= 80 && SpritePos.Y >= 200)*/ SpritePos.X = 530;

            if (SpritePos.X <= 455 && SpritePos.X >= 450 && SpritePos.Y > 280 && SpritePos.Y <= 550) SpritePos.X = 455;
            if (SpritePos.X >= 420 && SpritePos.X < 450 && SpritePos.Y > 280 && SpritePos.Y <= 550) SpritePos.X = 420;
            if (SpritePos.X >= 135 && SpritePos.X <= 140 && SpritePos.Y > 280 && SpritePos.Y <= 550) SpritePos.X = 135;
            if (SpritePos.X <= 175 && SpritePos.X >= 150 && SpritePos.Y > 280 && SpritePos.Y <= 550) SpritePos.X = 175;
            if (SpritePos.Y >= 270 && SpritePos.X > 135 && SpritePos.X < 455) SpritePos.Y = 270;
           // if (SpritePos.Y <= 320 && SpritePos.X > 135 && SpritePos.X < 455) SpritePos.Y = 320;


        }

        public void Update()
        {
            MoveSprite();
            HealthBar();
            ROBOT();
            Walls();

            if (SpritePos.Y >= 500 ) MenuState.IsShowLab1 = true;
          }
    }
}
