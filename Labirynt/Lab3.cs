﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin;

namespace Collin_Game.Levels
{
    class Lab3 : DrawableGameComponent
    {
        // private MainGame mainGame;
        private Game game;

        Texture2D Player;
        Vector2 SpritePos;
        Texture2D texApple;
        Vector2 pos_apple;
        public Rectangle rec_apple;

        Texture2D Robot;
        Vector2 RobotPos;
        Rectangle recRobot;

        Texture2D Room_2;
        Vector2 posRoom_2;
        Rectangle recRoom_2;


        Texture2D healthBar;
        Vector2 pos_healthBar;
        Rectangle rec_healthBar;
        public bool walkRobot = false;

        public Lab3 (Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }

        public override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            //create new objects

        }
        protected override void LoadContent()
        {
            Player = game.Content.Load<Texture2D>("collin2");
            Room_2 = game.Content.Load<Texture2D>("lab3");
            Robot = game.Content.Load<Texture2D>("robot");
            texApple = game.Content.Load<Texture2D>("Apple");
            pos_apple = new Vector2(50, 300);




            SpritePos = new Vector2(0,70);

            RobotPos = new Vector2(300, 300);

            recRoom_2.Height = 490;
            recRoom_2.Width = 625;


            healthBar = game.Content.Load<Texture2D>("Pasek_zycia");

            pos_healthBar = new Vector2(10, 450);
            // healthBar.Width = 150
            // healthBar.Height = 20
            rec_healthBar = new Rectangle(0, 0, 150, 15);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            spriteBatch.Draw(Room_2, recRoom_2, Color.White);

            spriteBatch.Draw(Robot, RobotPos, Color.White);
            spriteBatch.Draw(Player, SpritePos, Color.White);

            spriteBatch.Draw(healthBar, pos_healthBar, rec_healthBar, Color.White);
            spriteBatch.End();

        }
        private void EatApple()
        {

            if (SpritePos.X >= 70 && SpritePos.X <= 130 && SpritePos.Y >= 50 && SpritePos.Y <= 140)
            {
                rec_apple = new Rectangle(0, 0, 0, 0);
                rec_healthBar.Width = +150;
            }
        }
        private void ROBOT()
        {
            if (walkRobot == false)
            { RobotPos.X += 1; }


            if (RobotPos.X >= 400)
            {
                walkRobot = true;
                RobotPos.X = 400;
            }

            if (walkRobot == true)
            {
                RobotPos.X -= 1;
            }
            if (RobotPos.X <= 150)
            {
                walkRobot = false;
                RobotPos.X = 150;
            }

            if (SpritePos.X == RobotPos.X && SpritePos.Y == RobotPos.Y) rec_healthBar.Width -= 10;
            if (SpritePos.X <= RobotPos.X + 50 && SpritePos.X >= RobotPos.X - 20 && SpritePos.Y <= RobotPos.Y + 50 && SpritePos.Y >= RobotPos.Y - 30) rec_healthBar.Width -= 6;
        }
        private void MoveSprite()
        {
            KeyboardState currKey = Keyboard.GetState();
            if (currKey.IsKeyDown(Keys.Left)) SpritePos.X -= 2;
            if (currKey.IsKeyDown(Keys.Right)) SpritePos.X += 2;
            if (currKey.IsKeyDown(Keys.Up)) SpritePos.Y -= 2;
            if (currKey.IsKeyDown(Keys.Down)) SpritePos.Y += 2;
        }
        private void HealthBar()
        {


            if (rec_healthBar.Width >= 150)
            {
                rec_healthBar.Width = 150;
            }

            if (rec_healthBar.Width <= 0)
            {
                rec_healthBar.Width = 0;

                if (rec_healthBar.Width == 0)
                {
                    MenuState.IsShowGameOverScene = true;
                    rec_healthBar.Width = 150;
                    SpritePos = new Vector2(50, 50);
                }


            }
        }

        public void Update()
        {
            MoveSprite();
            HealthBar();
            ROBOT();
            EatApple();


            if (SpritePos.X <= 0) MenuState.IsShowLab2 = true;
            if (SpritePos.Y >= 450) MenuState.IsShowLab4 = true;
            if (SpritePos.X <= 6 && SpritePos.Y >= 210) SpritePos.X = 6;
            if (SpritePos.X >= 560) SpritePos.X = 560;

            if (SpritePos.Y >= 430 && SpritePos.X > 0 && SpritePos.X < 165) SpritePos.Y = 430;


            if (SpritePos.Y <= 90 && SpritePos.X > 0 && SpritePos.X < 300) SpritePos.Y = 90;
            if (SpritePos.X <= 305 && SpritePos.X > 300 && SpritePos.Y < 90) SpritePos.X = 305;

            if (SpritePos.X >= 470 &&  SpritePos.Y < 140 && SpritePos.X <= 480) SpritePos.X = 470;
            if (SpritePos.X <= 510 && SpritePos.X >= 480 && SpritePos.Y < 140) SpritePos.X = 510;

            if ( SpritePos.Y >= 190 && SpritePos.Y <= 200 && SpritePos.X > 0 && SpritePos.X < 170) SpritePos.Y = 190;
            if (SpritePos.Y <= 235&& SpritePos.Y >= 230 && SpritePos.X > 0 && SpritePos.X < 170) SpritePos.Y = 235;

            if (SpritePos.X <= 165 && SpritePos.X >= 150 && SpritePos.Y >= 200 && SpritePos.Y <= 320) SpritePos.X = 165;
            if (SpritePos.X <= 165 && SpritePos.X >= 150 && SpritePos.Y >= 360 && SpritePos.Y < 560) SpritePos.X = 165;
            if (SpritePos.X >= 130 && SpritePos.X <= 145 && SpritePos.Y >= 200 && SpritePos.Y < 320) SpritePos.X = 130;
            if (SpritePos.X >= 130 && SpritePos.X <= 145 && SpritePos.Y >= 360 && SpritePos.Y < 560) SpritePos.X = 130;

            if (SpritePos.X >= 250 && SpritePos.X <= 255 && SpritePos.Y >= 350 && SpritePos.Y <= 560) SpritePos.X = 250;
            if (SpritePos.X <= 280 && SpritePos.X >= 275 && SpritePos.Y >= 350 && SpritePos.Y <= 560) SpritePos.X = 280;
            if (SpritePos.X >= 280 && SpritePos.Y >= 430) SpritePos.Y = 430;
            //   if (SpritePos.Y <= 0) MenuState.Prison = true;



        }
    }
}
