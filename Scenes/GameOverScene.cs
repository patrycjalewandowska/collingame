﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Collin
{
    class GameOverScene : DrawableGameComponent
    {
        private MainGame mainGame;
        private Game game;
     //  Game game;
        Texture2D PlayButtonTexture;
        Texture2D ExitButtonTexture;
        Texture2D OverGameTexture;

        Rectangle recPlayButton;
        Rectangle recExitButton;
        Rectangle recOverGame;

        Color PlayButtonColor = Color.White;
        Color ExitButtonColor = Color.White;

        MouseState mouseState;
        Rectangle Cursor;

        bool plansza1 = false;


        public GameOverScene(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }
        protected override void LoadContent()
        {
            PlayButtonTexture = game.Content.Load<Texture2D>("newGame");
            ExitButtonTexture = game.Content.Load<Texture2D>("exit2");
            OverGameTexture = game.Content.Load<Texture2D>("GameOver");
           
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            spriteBatch.Draw(OverGameTexture, recOverGame, Color.White);
            spriteBatch.Draw(PlayButtonTexture, recPlayButton, PlayButtonColor);
            spriteBatch.Draw(ExitButtonTexture, recExitButton, ExitButtonColor);

            spriteBatch.End();
            if(plansza1 == true)
            {
                GraphicsDevice.Clear(Color.Black);
                MenuState.IsShowMainMenuScene = true;
            }

        }

        public void Update()
        {
           
            CalculateItemsPositions();
            CalculateItemsSize();
            UpdateCursorPosition();
            ButtonsEvents();
        }
        private void CalculateItemsSize()
        {
            /*Calculate buttons size */
            int Height = GraphicsDevice.Viewport.Height / 7;
            int Width = GraphicsDevice.Viewport.Width / 4;
            recPlayButton.Height = Height;
            recPlayButton.Width = Width;
            //
            recExitButton.Height = Height;
            recExitButton.Width = Width;
            /* Calculate logo size */
            recOverGame.Height = GraphicsDevice.Viewport.Height / 4;
            recOverGame.Width = GraphicsDevice.Viewport.Width / 2;
            //
        }
        private void CalculateItemsPositions()
        {
            recOverGame.X = GraphicsDevice.Viewport.Width / 2 - recOverGame.Width / 2;
            /* Calculate button position */
            int positionx = GraphicsDevice.Viewport.Width / 2 - recPlayButton.Width / 2;
            //
            recPlayButton.X = positionx;
            recPlayButton.Y = recOverGame.Height + recPlayButton.Height / 3;
            //
            //
            recExitButton.X = positionx;
            recExitButton.Y = recPlayButton.Y + 12 * recPlayButton.Height / 10;
            //      
        }
        private void UpdateCursorPosition()
        {
            /* Update Cursor position by Mouse */
            mouseState = Mouse.GetState();
            Cursor.X = mouseState.X; Cursor.Y = mouseState.Y;
        }
        private void ButtonsEvents()
        {
            if ((recPlayButton.Intersects(Cursor)))
            {
                PlayButtonColor = Color.Pink;
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    PlayButtonColor = Color.Red;
                    MenuState.IsShowMainMenuScene = true;
                    
                  //  MenuState.IsShowLab1 = true;
                    plansza1 = true;
                    
                   
                   
                    game.Window.AllowUserResizing = false; ;
                }
            }
            else
                PlayButtonColor = Color.White;

            if ((recExitButton.Intersects(Cursor)))
            {
                ExitButtonColor = Color.Pink;
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    ExitButtonColor = Color.Red;
                    game.Exit();
                }
            }
            else
                ExitButtonColor = Color.White;
        }
    }
}
