﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin_Game;
using Collin_Game.Levels;
using System.Diagnostics;

namespace Collin 
{
    class GameScene : DrawableGameComponent
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public static int ScreenHeight;
        public static int ScreenWidth;

        SpriteFont inf;


        Texture2D kluczyk;
        Vector2 pos_key;
        Rectangle rec_key;


        Texture2D Robot;
        Vector2 RobotPos;
        Rectangle recRobot;

        public bool COS = false;

        private MainGame mainGame;
        private Game game;

        Texture2D texPlayer;
        Texture2D col_up2;
        public Rectangle recPlayer;

        KeyboardState currKey;
       public  Vector2 SpritePos;


        private List<Texture2D> collin_left;
        Texture2D wiadomosc;
        Rectangle recWiadomosc;
        Vector2 posWiadomosc;     

        Texture2D texRoom1;
        Rectangle recRoom1;

        Texture2D healthBar;
        Vector2 pos_healthBar;
        Rectangle rec_healthBar;

        Texture2D magicBar;
        Vector2 pos_magicBar;
        Rectangle rec_magicBar;
        Color color_magic = Color.Blue;

        Texture2D energy;
        Vector2 pos_energy;
        Rectangle rec_energy;

        Texture2D texApple;
        Vector2 pos_apple;
       public  Rectangle rec_apple;

        public GameScene(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }
        public override void Initialize()
        {
         
          
            ScreenHeight = graphics.PreferredBackBufferHeight;

            ScreenWidth = graphics.PreferredBackBufferWidth;
            // TODO: Add your initialization logic here
            base.Initialize();
          //create new objects

        }


        protected override void LoadContent()
        {
            

            spriteBatch = new SpriteBatch(GraphicsDevice);

            //ładowanie tekstury

            texRoom1 = game.Content.Load<Texture2D>("room_1");
        
            texPlayer = game.Content.Load<Texture2D>("collin2");
            col_up2 = game.Content.Load<Texture2D>("colup_2");
            inf = game.Content.Load<SpriteFont>("Font");

            

            SpritePos = new Vector2(200, 200);

            //wiadomosc = game.Content.Load<Texture2D>("newButton");
            //recWiadomosc = new Rectangle(0, 0, 160, 40);
            ////posWiadomosc = new Vector2(400, 400);

            healthBar = game.Content.Load<Texture2D>("Pasek_zycia");
            Robot = game.Content.Load<Texture2D>("robot");
            RobotPos = new Vector2(400, 200);
            texApple = game.Content.Load<Texture2D>("Apple");

            //energy = game.Content.Load<Texture2D>("energyy");
            //pos_energy = new Vector2(300, 300);

            //magicBar = game.Content.Load<Texture2D>("magicBar");
            //pos_magicBar = new Vector2(400, 450);
            //rec_magicBar = new Rectangle(0, 0, 150, 15);


            kluczyk = game.Content.Load<Texture2D>("key");
            rec_key = new Rectangle(0, 0, 50, 100);

            pos_healthBar = new Vector2(10, 450);
            // healthBar.Width = 150
            // healthBar.Height = 20
            rec_healthBar = new Rectangle(0, 0, 150, 15);

            pos_apple = new Vector2(100, 100);
            rec_apple = new Rectangle(0, 0, 50, 50);
            //ustawienie wysokości i szerokości poruszanego obiektu


            recPlayer.X = 50;
            recPlayer.Y= 50;


            recRoom1.Height = 500;
            recRoom1.Width = 600;
            //ustawienie początkowej pozycji XY obiektu
            recRoom1.X = 0;
            recRoom1.Y = 0;
            
              recPlayer.Height = 200;
              recPlayer.Width = 100;

            
        }
        public void Update()
        {

            ///odczyt stanu klawiatury i w zależności od wciskanego przycisku poruszanie obiektem
          
            UpdateCollin();
            MoveSprite();
            LimitWalls();

           
            Health();
            Go();
            EatApple();
            //  Magic();
           if (SpritePos.X == RobotPos.X && SpritePos.Y == RobotPos.Y) rec_healthBar.Width -= 10;
            if (SpritePos.X <= RobotPos.X + 50 && SpritePos.X >= RobotPos.X - 20 && SpritePos.Y <= RobotPos.Y + 50 && SpritePos.Y >= RobotPos.Y - 30) rec_healthBar.Width -= 10;
            if (Keyboard.GetState().IsKeyDown(Keys.Q))
            {
                rec_key = new Rectangle(0, 0, 0, 0);
                COS = true;
            }
            

              if ((rec_apple.Intersects(recPlayer))) SpritePos.Y = 200;
           }
       
        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
           GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();
            
            //DODWANIE KOLEJNYCH MAP
            //sprteBatch.Draw(tekstura , pozycja X i Y , rozmiar tekstury , kolor);
            //spriteBatch.Draw(texRoom1,new Vector2(450,50), new Rectangle(0,0,500,300), Color.White);
            spriteBatch.Draw(texRoom1, recRoom1, Color.White);
            spriteBatch.Draw(texApple, pos_apple,rec_apple, Color.White);
            spriteBatch.Draw(kluczyk, new Vector2(400,100),rec_key, Color.White);
          //  spriteBatch.Draw(energy, pos_energy, rec_energy,  Color.White);
            spriteBatch.Draw(texPlayer, SpritePos, Color.White);
            spriteBatch.Draw(healthBar, pos_healthBar, rec_healthBar, Color.White);
            if(COS == false)
            { if (SpritePos.Y>= 350)
                {   spriteBatch.DrawString(inf, " You need a key", new Vector2(400, 400), Color.Black);}

            }
          
         // spriteBatch.Draw(Robot, RobotPos, Color.White);
            // spriteBatch.Draw(magicBar, pos_magicBar, rec_magicBar, color_magic);
            //if (rec_magicBar.Width <= 0)  spriteBatch.Draw(wiadomosc, posWiadomosc,recWiadomosc,Color.White);

            spriteBatch.End();
        }
        private void EatApple()
        {

            if (SpritePos.X >= 70 && SpritePos.X <= 130 && SpritePos.Y >= 50 && SpritePos.Y <= 140)
            {
                rec_apple = new Rectangle(0, 0, 0, 0);
                rec_healthBar.Width = +150;
            }
        }
        private void Go()
        {
            if (SpritePos.Y >= 380)

            {
                if (COS == true)
                {
                    if (SpritePos.Y >= 400 && SpritePos.X >= 260 && SpritePos.X <= 290)
                    {
                        MenuState.IsShowLevel_2 = true;
                        //  MenuState.Forest_2 = true;
                        //  MenuState.Prison = true;
                         }

                    }

                    else if (COS == false) SpritePos.Y = 380;

                }

            }
        
        //private void Magic()
        //{
        //    if (Keyboard.GetState().IsKeyDown(Keys.W))
        //    {
        //        pos_energy.X = SpritePos.X;
        //        pos_energy.Y = SpritePos.Y;
        //        rec_energy = new Rectangle(0, 0, 100, 100);

        //        rec_magicBar.Width -= 5;

        //    }


        //    pos_energy.X += 3;

        //    if (rec_magicBar.Width >= 150) rec_magicBar.Width = 150;
        //    if (rec_magicBar.Width <= 0)
        //    {
        //        rec_magicBar.Width = 0;
        //        recWiadomosc = new Rectangle(0, 0, 160, 40);
        //        posWiadomosc = new Vector2(400, 420);
        //    }
        //    if (rec_magicBar.Width == 0) rec_energy = new Rectangle(0, 0, 0, 0);
        //} 

      
        private void MoveSprite()
        {
            KeyboardState currKey = Keyboard.GetState();
            if (currKey.IsKeyDown(Keys.Left)) SpritePos.X -= 2;
            if (currKey.IsKeyDown(Keys.Right)) SpritePos.X += 2;
            if (currKey.IsKeyDown(Keys.Up)) SpritePos.Y -= 2;
            if (currKey.IsKeyDown(Keys.Down)) SpritePos.Y += 2;

        }

        private void LimitWalls()  // ograniczenie mapy dla postaci
        {
            if (SpritePos.X <= 50) SpritePos.X = 50;

            if (SpritePos.Y <= 45) SpritePos.Y = 45;

            if (SpritePos.X >= 500) SpritePos.X = 500;

            if (SpritePos.Y >= 380 && (SpritePos.X <= 260 || SpritePos.X >= 290)) SpritePos.Y = 380;
        }
        private void UpdateCollin()
        {

            // mouseState = Mouse.GetState();
            //Cursor.X = mouseState.X; Cursor.Y = mouseState.Y;
 }

        private void Health()
        {

            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                rec_apple = new Rectangle(0, 0, 0, 0);
                rec_healthBar.Width = +150;
            }

          if (Keyboard.GetState().IsKeyDown(Keys.Space)) rec_healthBar.Width -= 1;
            
            else
            {
                // rec_healthBar.Width += 1;
                if (rec_healthBar.Width >= 150) rec_healthBar.Width = 150;
                
                if (rec_healthBar.Width <= 0)
                {
                    rec_healthBar.Width = 0;

                    if (rec_healthBar.Width == 0)
                    {
                        MenuState.IsShowGameOverScene = true;
                        rec_healthBar.Width = 150;
                        SpritePos = new Vector2(50, 50);
                    }

                }

            }
        }
 }
}

