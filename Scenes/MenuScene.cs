﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Collin
{
    class MenuScene : DrawableGameComponent
    {
        Game game;
        SpriteFont napis;
        SpriteFont strzalka_gora;
        SpriteFont nazwagry;

        Texture2D PlayButtonTexture;
        Texture2D ExitButtonTexture;
        Texture2D LogoTexture;
        Texture2D Controls;

        Rectangle recPlayButton;
        Rectangle recExitButton;
        Rectangle recLogo;
        Rectangle recControls;
        

        Color PlayButtonColor = Color.White;
        Color ExitButtonColor = Color.White;
        Color ControlsColor = Color.White;

        MouseState mouseState;
        Rectangle Cursor;


        bool skok = false;

        Texture2D Button_Back;
        KeyboardState currKey;
        Rectangle rec_Back;

         Color BackColor = Color.White;




        public MenuScene(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }

        protected override void LoadContent()
        {
            PlayButtonTexture = game.Content.Load<Texture2D>("newButton");
            ExitButtonTexture = game.Content.Load<Texture2D>("newButton");
            LogoTexture = game.Content.Load<Texture2D>("tloMenu");
            Controls = game.Content.Load<Texture2D>("newButton");
            Button_Back = game.Content.Load<Texture2D>("newButton");
            napis = game.Content.Load<SpriteFont>("Font");
            strzalka_gora = game.Content.Load<SpriteFont>("Font");
            nazwagry = game.Content.Load<SpriteFont>("nazwagry");
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
           
            if (skok == false)
            {
                GraphicsDevice.Clear(Color.Black);
                spriteBatch.Begin();
                spriteBatch.Draw(LogoTexture, recLogo, Color.White);
                spriteBatch.Draw(Controls, recControls, ControlsColor);
              
                spriteBatch.Draw(PlayButtonTexture, recPlayButton, PlayButtonColor);
                spriteBatch.Draw(ExitButtonTexture, recExitButton, ExitButtonColor);


                spriteBatch.DrawString(napis, "New Game", new Vector2(405, 250), Color.Black);
                spriteBatch.DrawString(napis, "Controls", new Vector2(420, 340), Color.Black);
                spriteBatch.DrawString(napis, "Exit ", new Vector2(435, 430), Color.Black);
                spriteBatch.DrawString(nazwagry, "CollinGame", new Vector2(0,10), Color.WhiteSmoke);
                spriteBatch.End();
            }
                 if ( skok == true)
            {

                GraphicsDevice.Clear(Color.Black);
                spriteBatch.Begin();
                spriteBatch.Draw(Button_Back, rec_Back, BackColor);
                spriteBatch.DrawString(napis, "Back", new Vector2(410, 30), Color.Black);
                spriteBatch.DrawString(napis, "Sterowanie:", new Vector2(0, 10), Color.WhiteSmoke);
                spriteBatch.DrawString(napis, "Strzalka w gore - ruch postaci w gore", new Vector2(0, 30), Color.WhiteSmoke);
                spriteBatch.DrawString(napis, "Strzalka w dol - ruch postaci w dol", new Vector2(0, 50), Color.WhiteSmoke);
                spriteBatch.DrawString(napis, "Strzalka w lewo- ruch postaci w lewo", new Vector2(0, 70), Color.WhiteSmoke);
                spriteBatch.DrawString(napis, "Strzalka w prawo- ruch postaci w prawo", new Vector2(0, 90), Color.WhiteSmoke);
                spriteBatch.DrawString(napis, "Przycisk Q - zbieranie przedmiotow", new Vector2(0, 110), Color.WhiteSmoke);
                spriteBatch.DrawString(napis, "Celem gry jest doprowadzenie Collina do Farmy. W domu porywacza poruszaja sie ", new Vector2(0, 170), Color.WhiteSmoke);
                spriteBatch.DrawString(napis, "NPC przeciwnika przed ktorymi musisz uciekac. W roznych miejscach sa schowane ", new Vector2(0, 190), Color.WhiteSmoke);
                spriteBatch.DrawString(napis, "jablka, ktore odradzaja Ci zycie.", new Vector2(0, 210), Color.WhiteSmoke);

                spriteBatch.DrawString(napis, " ", new Vector2(120, 50), Color.WhiteSmoke);

                spriteBatch.End();

            }
 }

        public void Update()
        {
            CalculateItemsPositions();
            CalculateItemsSize();
            UpdateCursorPosition();
            ButtonsEvents();
 }
        private void CalculateItemsSize()
        {
            int Height = GraphicsDevice.Viewport.Height / 7;
            int Width = GraphicsDevice.Viewport.Width / 4;

            recPlayButton.Height = Height ;
            recPlayButton.Width = Width;
            
            recExitButton.Height = Height;
            recExitButton.Width = Width;
            // CONTROLS
            recControls.Height = Height;
            recControls.Width = Width;
            
            rec_Back.Height = Height;
            rec_Back.Width = Width;
          
            recLogo.Height = GraphicsDevice.Viewport.Height ;
            recLogo.Width = GraphicsDevice.Viewport.Width ;
            
        }

        private void CalculateItemsPositions()
        {
           // recLogo.X = GraphicsDevice.Viewport.Width / 2 - recLogo.Width / 2;
          
            int positionx = GraphicsDevice.Viewport.Width / 2 - recPlayButton.Width / 2;
            //
            int positiony= GraphicsDevice.Viewport.Width / 2 - recPlayButton.Width / 2;
            recPlayButton.X = positionx + 150;
            recPlayButton.Y = positiony;
            //
            //
            recExitButton.X = recPlayButton.X;
            recExitButton.Y = recControls.Y + 90;// recPlayButton.Height / 10;
            //      
            recControls.X = recPlayButton.X;
            recControls.Y = recPlayButton.Y + 90;


            if(skok == true)
            { 
                rec_Back.X = GraphicsDevice.Viewport.Width / 2 + 50 ;
                rec_Back.Y = GraphicsDevice.Viewport.Height / 100  ;

            }
        }

        private void UpdateCursorPosition()
        {
           
            mouseState = Mouse.GetState();
            Cursor.X = mouseState.X; Cursor.Y = mouseState.Y;
        }
        private void ButtonsEvents()
        {
            if ((recPlayButton.Intersects(Cursor)))
            {
                PlayButtonColor = Color.Pink;
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                   
                    PlayButtonColor = Color.Red;
                    MenuState.IsShowGameScene = true;
                    game.Window.AllowUserResizing = false;
                    if( skok == true)
                    {
                        MenuState.IsShowMainMenuScene = true;
                        skok = false;
                    }
                }
            }
            else
                PlayButtonColor = Color.White;

            if ((recExitButton.Intersects(Cursor)))
            {
                ExitButtonColor = Color.Pink;
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    game.Exit();
                }
            }
            else
                ExitButtonColor = Color.White;

            if ((rec_Back.Intersects(Cursor)))
            {
                BackColor = Color.Pink;

                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    skok = false;
                }
            }
            else
                BackColor = Color.White;

            

            if ((recControls.Intersects(Cursor)))
            {
                ControlsColor = Color.Pink;
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    skok = true;

                   // MenuState.IsShowControlsScene = true;
                    game.Window.AllowUserResizing = false; ;
                }
            }
            else
                ControlsColor = Color.White;
        }
    }
}