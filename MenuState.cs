﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collin
    //będziemy jej używać do sprawdzania jaka scena jest aktualnie wyświetlana i przełączania tychże scen
{
    public class MenuState
    {
        private static bool _IsShowMainMenuScene;
        private static bool _IsShowGameScene;
        private static bool _IsShowGameOverScene;
      

        private static bool _IsShowLevel_2;
        private static bool _IsShowLevel_3;
        private static bool _IsShowLevel_4;
        private static bool _IsShowLevel_5;
        private static bool _IsShowLevel_6;
        private static bool _IsShowLevel_7;
        private static bool _IsShowLevel_8;

        private static bool _IsShowLab1;
        private static bool _IsShowLab2;
        private static bool _IsShowLab3;
        private static bool _IsShowLab4;

        private static bool _Prison;

        private static bool _Forest_1;
        private static bool _Forest_2;
        


        public static bool IsShowMainMenuScene { get { return _IsShowMainMenuScene; } set { if (value == true) { _IsShowGameScene = false; _IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowMainMenuScene = value; } }
        public static bool IsShowGameScene { get { return _IsShowGameScene; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowGameScene = value; } }
        public static bool IsShowGameOverScene { get { return _IsShowGameOverScene; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false; } _IsShowGameOverScene = value; } }
       

        public static bool IsShowLevel_2 { get { return _IsShowLevel_2; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowLevel_2 = value; } }
        public static bool IsShowLevel_3 { get { return _IsShowLevel_3; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowLevel_3 = value; } }
        public static bool IsShowLevel_4 { get { return _IsShowLevel_4; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowLevel_4 = value; } }
        public static bool IsShowLevel_5 { get { return _IsShowLevel_5; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowLevel_5 = value; } }
        public static bool IsShowLevel_6 { get { return _IsShowLevel_6; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowLevel_6 = value; } }
        public static bool IsShowLevel_7 { get { return _IsShowLevel_7; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowLevel_7 = value; } }
        public static bool IsShowLevel_8 { get { return _IsShowLevel_8; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowLevel_8 = value; } }
        public static bool IsShowLab1    { get { return _IsShowLab1; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowLab1 = value; } }
        public static bool IsShowLab2    { get { return _IsShowLab2; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowLab2 = value; } }
        public static bool IsShowLab3    { get { return _IsShowLab3; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _IsShowLab3 = value; } }
        public static bool IsShowLab4    { get { return _IsShowLab4; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _Prison = false; _Forest_2 = false;  } _IsShowLab4 = value; } }
        public static bool Prison        { get { return _Prison; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Forest_2 = false;  } _Prison = value; } }
        public static bool Forest_1      { get { return _Forest_1; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_2 = false;  } _Forest_1 = value; } }
        public static bool Forest_2 { get { return _Forest_2; } set { if (value == true) { _IsShowMainMenuScene = false; _IsShowGameScene = false; IsShowGameOverScene = false; _IsShowLevel_2 = false; _IsShowLevel_3 = false; _IsShowLevel_4 = false; _IsShowLevel_5 = false; _IsShowLevel_6 = false; _IsShowLevel_7 = false; _IsShowLevel_8 = false; _IsShowLab1 = false; _IsShowLab2 = false; _IsShowLab3 = false; _IsShowLab4 = false; _Prison = false; _Forest_1 = false;   } _Forest_2 = value; } }
        



    }
}