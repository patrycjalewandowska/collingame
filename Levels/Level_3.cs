﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin;

namespace Collin_Game.Levels
{
    class Level_3 : DrawableGameComponent
    {
        // private MainGame mainGame;
        private Game game;
        bool XwalkRobot = true;
        bool YwalkRobot = true;
        Texture2D Robot;
        Vector2 RobotPos;
        Vector2 RobotPos2 ;
        Rectangle recRobot;

        Texture2D Player;
        Vector2 SpritePos;

        Texture2D kluczyk;
        Vector2 pos_key;
        Rectangle rec_key;

        Texture2D Room_3;
        Vector2 posRoom_3;
        Rectangle recRoom_3;


        Texture2D healthBar;
        Vector2 pos_healthBar;
        Rectangle rec_healthBar;

        public Level_3(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }

        public override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            //create new objects

        }
        protected override void LoadContent()
        {
            Player = game.Content.Load<Texture2D>("collin2");
            Room_3 = game.Content.Load<Texture2D>("room_3");
            kluczyk = game.Content.Load<Texture2D>("key");
            Robot = game.Content.Load<Texture2D>("robot");

            RobotPos = new Vector2(400, 300);

            rec_key = new Rectangle(0, 0, 50, 100);


            SpritePos = new Vector2(0, 50);

            recRoom_3.Height = 490;
            recRoom_3.Width = 625;

            healthBar = game.Content.Load<Texture2D>("Pasek_zycia");

            pos_healthBar = new Vector2(10, 450);
            // healthBar.Width = 150
            // healthBar.Height = 20
            rec_healthBar = new Rectangle(0, 0, 150, 15);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            spriteBatch.Draw(Room_3, recRoom_3, Color.White);
            spriteBatch.Draw(Robot, RobotPos, Color.White);
            spriteBatch.Draw(Robot, RobotPos2, Color.White);

            spriteBatch.Draw(Player, SpritePos, Color.White);

            spriteBatch.Draw(healthBar, pos_healthBar, rec_healthBar, Color.White);
            spriteBatch.Draw(kluczyk, new Vector2(400, 100), rec_key, Color.White);
            spriteBatch.End();

        }
        private void MoveSprite()
        {
            KeyboardState currKey = Keyboard.GetState();
            if (currKey.IsKeyDown(Keys.Left)) SpritePos.X -= 2;
            if (currKey.IsKeyDown(Keys.Right)) SpritePos.X += 2;
            if (currKey.IsKeyDown(Keys.Up)) SpritePos.Y -= 2;
            if (currKey.IsKeyDown(Keys.Down)) SpritePos.Y += 2;

        }
        private void HealthBar()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                rec_healthBar.Width -= 1;

            }
            else
            {
                rec_healthBar.Width += 1;
                if (rec_healthBar.Width >= 150)
                {
                    rec_healthBar.Width = 150;
                }

                if (rec_healthBar.Width <= 0)
                {
                    rec_healthBar.Width = 0;

                    if (rec_healthBar.Width == 0)
                    {
                        MenuState.IsShowGameOverScene = true;
                        rec_healthBar.Width = 150;
                        SpritePos = new Vector2(50, 50);
                    }

                }
            }
        }
        private void LimitsWalls()
        {
            if (SpritePos.X <= 50 && (SpritePos.Y <= 200 || SpritePos.Y >= 250)) SpritePos.X = 50; 

            if (SpritePos.X <= 40)
            {

                MenuState.IsShowLevel_2 = true;

            }

            if (SpritePos.X >= 520) SpritePos.X = 520;
            if (SpritePos.Y >= 380) SpritePos.Y = 380;


        }


        public void Update()
        {
            HealthBar();
            MoveSprite();
            LimitsWalls();
            ROBOT();

            if (Keyboard.GetState().IsKeyDown(Keys.Q))
            {
              //  rec_key = new Rectangle(0, 0, 0, 0);
             
            }

        }
        private void ROBOT()
        {
            if (XwalkRobot == true && YwalkRobot == true) RobotPos.X -= 2;
            if (RobotPos.X == 100) XwalkRobot = false;
            if (XwalkRobot == false && YwalkRobot == true) RobotPos.X += 2;
            if (RobotPos.X == 400) YwalkRobot = false;
            if (XwalkRobot == false && YwalkRobot == false) RobotPos.Y -= 2;
            if (RobotPos.Y == 100) XwalkRobot = true;
            if (XwalkRobot == true && YwalkRobot == false)
            {
                RobotPos.Y += 2;
                if (RobotPos.X == 400 && RobotPos.Y == 300)
                { YwalkRobot = true; }
            }

            if (SpritePos.X == RobotPos.X && SpritePos.Y == RobotPos.Y) rec_healthBar.Width -= 10;
            if (SpritePos.X <= RobotPos.X + 50 && SpritePos.X >= RobotPos.X - 20 && SpritePos.Y <= RobotPos.Y + 50 && SpritePos.Y >= RobotPos.Y - 30) rec_healthBar.Width -= 6;
        }

    }
}
