﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin;

namespace Collin_Game.Levels
{
    class Level_2 : DrawableGameComponent
    {
        // private MainGame mainGame;

       
        bool XwalkRobot = true;
        bool YwalkRobot = true;
        private Game game;

        Texture2D Player;
        Vector2 SpritePos;

        Texture2D Robot;
        Vector2 RobotPos;
        Rectangle recRobot;

        Texture2D Room_2;
        Vector2 posRoom_2;
        Rectangle recRoom_2;

        bool stop = true;


        Texture2D healthBar;
        Vector2 pos_healthBar;
        Rectangle rec_healthBar;
        public static Random Random;

        SpriteFont inf;

        public Level_2(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }

        public override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            //create new objects

        }
        protected override void LoadContent()
        {
            Player = game.Content.Load<Texture2D>("collin2");
            Room_2 = game.Content.Load<Texture2D>("room_2r");
            Robot = game.Content.Load<Texture2D>("robot");
            inf = game.Content.Load<SpriteFont>("Font");



            SpritePos = new Vector2(275, 60);

            RobotPos = new Vector2(400, 300);

            recRoom_2.Height = 490;
            recRoom_2.Width = 625;


            healthBar = game.Content.Load<Texture2D>("Pasek_zycia");


            pos_healthBar = new Vector2(10, 450);
            // healthBar.Width = 150
            // healthBar.Height = 20
            rec_healthBar = new Rectangle(0, 0, 150, 15);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            spriteBatch.Draw(Room_2, recRoom_2, Color.White);
            
            spriteBatch.Draw(Robot, RobotPos, Color.White);
            spriteBatch.Draw(Player, SpritePos, Color.White);

            spriteBatch.Draw(healthBar, pos_healthBar, rec_healthBar, Color.White);

        if(SpritePos.Y >= 379)
            {
             //   spriteBatch.DrawString(inf, "Find key", new Vector2(400, 400), Color.Black);
            }

            spriteBatch.End();

        }
        private void MoveSprite()
        {
            KeyboardState currKey = Keyboard.GetState();
            if (currKey.IsKeyDown(Keys.Left)) SpritePos.X -= 2;
            if (currKey.IsKeyDown(Keys.Right)) SpritePos.X += 2;
            if (currKey.IsKeyDown(Keys.Up)) SpritePos.Y -= 2;
            if (currKey.IsKeyDown(Keys.Down)) SpritePos.Y += 2;

        }
        private void HealthBar()
        {
            
              
                if (rec_healthBar.Width >= 150)
                {
                    rec_healthBar.Width = 150;
                }

                if (rec_healthBar.Width <= 0)
                {
                    rec_healthBar.Width = 0;

                    if (rec_healthBar.Width == 0)
                    {
                        MenuState.IsShowGameOverScene = true;
                        rec_healthBar.Width = 150;
                        SpritePos = new Vector2(50, 50);
                    }

                }
            
        }
        private void LimitsWalls()
        {
            if( stop == false )
            {
                if (SpritePos.Y >= 379) SpritePos.Y = 379;
            }
            if (SpritePos.Y <= 50 && SpritePos.X >= 260 && SpritePos.X <= 290)   MenuState.IsShowGameScene = true;
  
            if (SpritePos.Y <= 70 && (SpritePos.X <= 260 || SpritePos.X >= 290)) SpritePos.Y = 70;

            if (SpritePos.X >= 520 &&( SpritePos.Y >=200 || SpritePos.Y<= 250))  MenuState.IsShowLevel_3 = true;
           
            if (SpritePos.X >= 500 && ( SpritePos.Y <= 200 || SpritePos.Y >= 250)) SpritePos.X = 500;
            if( stop == true)
            {
                if (SpritePos.Y >= 380 && SpritePos.X >= 260 && SpritePos.X <= 290) MenuState.IsShowLevel_5 = true;
            }
                     
          

            if (SpritePos.Y >= 380 && (SpritePos.X <= 260 || SpritePos.X >= 290)) SpritePos.Y = 380;

            if (SpritePos.X < 44) SpritePos.X = 44;
           

        }
        private void ROBOT()
        {
            if (XwalkRobot == true && YwalkRobot == true) RobotPos.X -= 2;
            if (RobotPos.X == 100) XwalkRobot = false;
            if (XwalkRobot == false && YwalkRobot == true) RobotPos.X += 2;
            if (RobotPos.X == 400) YwalkRobot = false;
            if (XwalkRobot == false && YwalkRobot == false) RobotPos.Y -= 2;
            if (RobotPos.Y == 100) XwalkRobot = true;
            if (XwalkRobot == true && YwalkRobot == false)
            {
                RobotPos.Y += 2;
                if (RobotPos.X == 400 && RobotPos.Y == 300)
                { YwalkRobot = true; }
            }

            if (SpritePos.X == RobotPos.X && SpritePos.Y == RobotPos.Y) rec_healthBar.Width -= 10;
            if (SpritePos.X <= RobotPos.X + 50 && SpritePos.X >= RobotPos.X - 20 && SpritePos.Y <= RobotPos.Y + 50 && SpritePos.Y >= RobotPos.Y - 30) rec_healthBar.Width -= 6;
        }
       

        public void Update()
        {
            MoveSprite();
            LimitsWalls();
            HealthBar();
            ROBOT();

      }


    }
}
