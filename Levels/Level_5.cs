﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin;

namespace Collin_Game.Levels
{
    class Level_5 : DrawableGameComponent
    {
        // private MainGame mainGame;
        private Game game;

        Texture2D Player;
        Vector2 SpritePos;

        Texture2D Room_5;
        Vector2 posRoom_5;
        Rectangle recRoom_5;


        Texture2D healthBar;
        Vector2 pos_healthBar;
        Rectangle rec_healthBar;

        public Level_5(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }

        public override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            //create new objects

        }
        protected override void LoadContent()
        {
            Player = game.Content.Load<Texture2D>("collin2");
            Room_5 = game.Content.Load<Texture2D>("room_5");

      

            recRoom_5.Height = 490;
            recRoom_5.Width = 625;
            SpritePos = new Vector2(300, 40);

            healthBar = game.Content.Load<Texture2D>("Pasek_zycia");

            pos_healthBar = new Vector2(10, 450);
            // healthBar.Width = 150
            // healthBar.Height = 20
            rec_healthBar = new Rectangle(0, 0, 150, 15);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            spriteBatch.Draw(Room_5, recRoom_5, Color.White);
            spriteBatch.Draw(Player, SpritePos, Color.White);

            spriteBatch.Draw(healthBar, pos_healthBar, rec_healthBar, Color.White);
            spriteBatch.End();

        }
        private void MoveSprite()
        {
            KeyboardState currKey = Keyboard.GetState();
            if (currKey.IsKeyDown(Keys.Left)) SpritePos.X -= 2;
            if (currKey.IsKeyDown(Keys.Right)) SpritePos.X += 2;
            if (currKey.IsKeyDown(Keys.Up)) SpritePos.Y -= 2;
            if (currKey.IsKeyDown(Keys.Down)) SpritePos.Y += 2;

        }
        private void Health()
        {

            

            if (Keyboard.GetState().IsKeyDown(Keys.Space)) rec_healthBar.Width -= 1;

            else
            {
                // rec_healthBar.Width += 1;
                if (rec_healthBar.Width >= 150) rec_healthBar.Width = 150;

                if (rec_healthBar.Width <= 0)
                {
                    rec_healthBar.Width = 0;

                    if (rec_healthBar.Width == 0)
                    {
                        MenuState.IsShowGameOverScene = true;
                        rec_healthBar.Width = 150;
                        SpritePos = new Vector2(50, 50);
                    }

                }

            }
        }
        private void LimitsWalls()
        {

            if (SpritePos.X >= 570 && SpritePos.Y >= 190 && SpritePos.Y <= 220)  MenuState.IsShowLevel_4 = true;
   
            if (SpritePos.Y <= 30 && SpritePos.X >= 280 && SpritePos.X <= 310) MenuState.IsShowLevel_2 = true;

           if (SpritePos.X <= 20 && SpritePos.Y >= 190 && SpritePos.Y <= 220) MenuState.IsShowLevel_7 = true;

            if (SpritePos.Y >= 420 && SpritePos.X >= 280 && SpritePos.X <= 310)  MenuState.IsShowLevel_8 = true;
            
            if (SpritePos.Y <= 50 && (SpritePos.X <= 280 || SpritePos.X >= 310)) SpritePos.Y = 50;
            if (SpritePos.X <= 40 &&(SpritePos.Y <=190  || SpritePos.Y >= 220)) SpritePos.X = 40;
            if (SpritePos.Y >= 400 && (SpritePos.X <= 280 || SpritePos.X >= 310)) SpritePos.Y = 400;
            if (SpritePos.X >= 550 && (SpritePos.Y <= 190 || SpritePos.Y >= 220)) SpritePos.X = 550;

        }
        public void Update()
        {
            Health();
            MoveSprite();
            LimitsWalls();
         



        }
    }
}
