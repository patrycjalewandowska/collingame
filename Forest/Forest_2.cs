﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin;

namespace Collin_Game.Labirynt
{
    class Forest_2 : DrawableGameComponent
    {
       private MainGame mainGame;
         Game game;
        SpriteFont napis_koncowy;
        SpriteFont opis_koncowy;

        Texture2D Collin1;
        Texture2D Collin2;
        MouseState mouseState;
        Rectangle Cursor;

        Texture2D Player;
        Vector2 SpritePos;

        Texture2D Exit;
        Vector2 posExit;
        Rectangle recExit;
        Color colExit = Color.White;

        Texture2D lmap1;
        Vector2 poslmap1;
        Rectangle reclmap1;


        public Forest_2(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }

        public override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
           

           
            //create new objects

        }
        protected override void LoadContent()
        {
            Player = game.Content.Load<Texture2D>("collin2");
            SpritePos = new Vector2(100, 0);
            Exit = game.Content.Load<Texture2D>("newButton");
            Collin1 = game.Content.Load<Texture2D>("colleft_2");
            Collin2 = game.Content.Load<Texture2D>("colright_2");
            lmap1 = game.Content.Load<Texture2D>("Forest3");

            napis_koncowy = game.Content.Load<SpriteFont>("nazwagry");
            opis_koncowy = game.Content.Load<SpriteFont>("Font");


        }
       

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            spriteBatch.Draw(lmap1, new Vector2(70, 100), Color.White);
           

            spriteBatch.Draw(Player, new Vector2(250,120), Color.White);
            spriteBatch.Draw(Collin1, new Vector2(250, 220), Color.White);
            spriteBatch.Draw(Collin2, new Vector2(300, 320), Color.White);
            spriteBatch.Draw(Collin1, new Vector2(340, 280), Color.White);
            spriteBatch.Draw(Collin2, new Vector2(100, 260), Color.White);
            // spriteBatch.Draw(Exit,, recExit, colExit);

            spriteBatch.DrawString(napis_koncowy, " The End ", new Vector2(150, 10), Color.White);

            spriteBatch.DrawString(opis_koncowy, " Now, Collin is happy with his friends thakns to you.", new Vector2(130, 450), Color.White);
            spriteBatch.End();

        }



        public void Update()
        {
            CalculateItemsPositions();
            CalculateItemsSize();
            UpdateCursorPosition();
            ButtonsEvents();
        }
        private void CalculateItemsSize()
        {
            int Height = GraphicsDevice.Viewport.Height / 7 ;
            int Width = GraphicsDevice.Viewport.Width / 4 ;
            recExit.Height = Height;
            recExit.Width = Width;
        }

        private void CalculateItemsPositions()
        {


            int positionx = GraphicsDevice.Viewport.Width / 2 - recExit.Width / 2;

            int positiony = GraphicsDevice.Viewport.Width / 2 - recExit.Width / 2;
            posExit.X = positionx;
            posExit.Y = positiony + 200; ;
        }

        private void UpdateCursorPosition()
        {

            mouseState = Mouse.GetState();
            Cursor.X = mouseState.X; Cursor.Y = mouseState.Y;
        }
        private void ButtonsEvents()
        {
            if ((recExit.Intersects(Cursor)))
            {  colExit = Color.Pink;

                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    // game.Window.AllowUserResizing = false;
                  
                    game.Exit();
                }
                else
                    colExit = Color.White;
            }
        }
    }
}

