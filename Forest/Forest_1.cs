﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin;

namespace Collin_Game.Labirynt
{
    class Forest_1 : DrawableGameComponent
    {
        // private MainGame mainGame;
        private Game game;

        Texture2D Player;
        Vector2 SpritePos;

        Texture2D Robot;
        Vector2 RobotPos;
        Rectangle recRobot;

        Texture2D lmap1;
        Vector2 poslmap1;
        Rectangle reclmap1;

        public bool walkRobot = false;

        Texture2D kluczyk;
        Vector2 pos_key;
        Rectangle rec_key;


        Texture2D healthBar;
        Vector2 pos_healthBar;
        Rectangle rec_healthBar;

        public Forest_1(Game game) : base(game)
        {
            this.game = game;
            LoadContent();
        }

        public override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            //create new objects

        }
        protected override void LoadContent()
        {
            Player = game.Content.Load<Texture2D>("collin2");
            SpritePos = new Vector2(100, 0);

            Robot = game.Content.Load<Texture2D>("to_cos");
            RobotPos = new Vector2(270, 270);

            lmap1 = game.Content.Load<Texture2D>("to_moze_to");

            kluczyk = game.Content.Load<Texture2D>("key");
            //  rec_key = new Rectangle(0, 0, 0, 0);

            healthBar = game.Content.Load<Texture2D>("Pasek_zycia");

            pos_healthBar = new Vector2(10, 450);
            // healthBar.Width = 150
            // healthBar.Height = 20
            rec_healthBar = new Rectangle(0, 0, 150, 15);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            spriteBatch.Draw(lmap1, new Vector2(50, -50), Color.White);
            spriteBatch.Draw(Robot, RobotPos, Color.White);
            spriteBatch.Draw(Player, SpritePos, Color.White);

            spriteBatch.Draw(healthBar, pos_healthBar, rec_healthBar, Color.White);
            spriteBatch.End();

        }

        public void Update()
        {


            if (walkRobot == false)
            { RobotPos.X += 1; }


            if (RobotPos.X >= 400)
            {
                walkRobot = true;
                RobotPos.X = 400;
            }

            if (walkRobot == true)
            {
                RobotPos.X -= 1;
            }
            if (RobotPos.X <= 200)
            {
                walkRobot = false;
                RobotPos.X = 200;
            }

            KeyboardState currKey = Keyboard.GetState();
            if (currKey.IsKeyDown(Keys.Left)) SpritePos.X -= 2;
            if (currKey.IsKeyDown(Keys.Right)) SpritePos.X += 2;
            if (currKey.IsKeyDown(Keys.Up)) SpritePos.Y -= 2;
            if (currKey.IsKeyDown(Keys.Down)) SpritePos.Y += 2;



            if (SpritePos.Y >= 300) MenuState.Forest_2 = true;




            // PASEK ZYCIA
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                rec_healthBar.Width -= 1;
            }
            else
            {
                rec_healthBar.Width += 1;
                if (rec_healthBar.Width >= 150)
                {
                    rec_healthBar.Width = 150;
                }

                if (rec_healthBar.Width <= 0)
                {
                    rec_healthBar.Width = 0;

                    if (rec_healthBar.Width == 0)
                    {
                        MenuState.IsShowGameOverScene = true;
                        rec_healthBar.Width = 150;
                        SpritePos = new Vector2(50, 50);
                    }

                }
            }
        }
    }
}
