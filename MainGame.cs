﻿using Collin_Game.Labirynt;
using Collin_Game.Levels;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Collin
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MainGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        MenuScene menuScene;
        GameScene gameScene;
        GameOverScene gameOverScene;
      

        Level_2 level_2;
        Level_3 level_3;
        Level_4 level_4;
        Level_5 level_5;
        Level_6 level_6;
        Level_7 level_7;
        Level_8 level_8;

        Lab1 lab1;
        Lab2 lab2;
        Lab3 lab3;
        Lab4 lab4;

        Prison _prison;

        Forest_1 forest_1;
        Forest_2 forest_2;
        


        public MainGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        
            this.graphics.PreferredBackBufferHeight = 500;
            this.graphics.PreferredBackBufferWidth = 600;

        }
        protected override void Initialize()
        {
           
            base.Initialize();
           
            
            menuScene = new MenuScene(this);
            gameScene = new GameScene(this);
            gameOverScene = new GameOverScene(this);
        

            level_2 = new Level_2(this);
            level_3 = new Level_3(this);
            level_4 = new Level_4(this);
            level_5 = new Level_5(this);
            level_6 = new Level_6(this);
            level_7 = new Level_7(this);
            level_8 = new Level_8(this);

            lab1 = new Lab1(this);
            lab2 = new Lab2(this);
            lab3 = new Lab3(this);
            lab4 = new Lab4(this);

            _prison = new Prison(this);

            forest_1 = new Forest_1(this);
            forest_2 = new Forest_2(this);
          



            IsMouseVisible = true;
            Window.AllowUserResizing = true;
            // menu
            MenuState.IsShowGameScene = true;
            MenuState.IsShowGameOverScene = true;
            MenuState.IsShowMainMenuScene = true;
        
 }

        protected override void LoadContent()
        {
           
            spriteBatch = new SpriteBatch(GraphicsDevice);

           
        }

        
        protected override void UnloadContent()
        {
    
        }

        protected override void Update(GameTime gameTime) // jaka scena w danym momencie bedzie wyswietlana
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
           // tylko jedna scena
            if (MenuState.IsShowMainMenuScene)
                menuScene.Update();
            if (MenuState.IsShowGameScene)
                gameScene.Update();
            if (MenuState.IsShowGameOverScene)
                gameOverScene.Update();
          

            if  (MenuState.IsShowLevel_2)
                  level_2.Update();
            if (MenuState.IsShowLevel_3)
                level_3.Update();
            if (MenuState.IsShowLevel_4)
                level_4.Update();
            if (MenuState.IsShowLevel_5)
                level_5.Update();
            if (MenuState.IsShowLevel_6)
                level_6.Update();
            if (MenuState.IsShowLevel_7)
                level_7.Update();
            if (MenuState.IsShowLevel_8)
                level_8.Update();

            if (MenuState.IsShowLab1)
                lab1.Update();
            if (MenuState.IsShowLab2)
                lab2.Update();
            if (MenuState.IsShowLab3)
                lab3.Update();
            if (MenuState.IsShowLab4)
                lab4.Update();

            if (MenuState.Prison)
                _prison.Update();

            if (MenuState.Forest_1)
                forest_1.Update();
            if (MenuState.Forest_2)
                forest_2.Update();
           

            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
           
            if (MenuState.IsShowMainMenuScene)
                menuScene.Draw(spriteBatch, gameTime);
            if (MenuState.IsShowGameScene)
                gameScene.Draw(spriteBatch, gameTime);
            if (MenuState.IsShowGameOverScene)
                gameOverScene.Draw(spriteBatch, gameTime);
         

            if (MenuState.IsShowLevel_2)
                level_2.Draw(spriteBatch, gameTime);
            if (MenuState.IsShowLevel_3)
                level_3.Draw(spriteBatch, gameTime);
            if(MenuState.IsShowLevel_4)
                level_4.Draw(spriteBatch, gameTime);
            if (MenuState.IsShowLevel_5)
                level_5.Draw(spriteBatch, gameTime);
            if (MenuState.IsShowLevel_6)
                level_6.Draw(spriteBatch, gameTime);
            if (MenuState.IsShowLevel_7)
                level_7.Draw(spriteBatch, gameTime);
            if (MenuState.IsShowLevel_8)
                level_8.Draw(spriteBatch, gameTime);

            if (MenuState.IsShowLab1)
                lab1.Draw(spriteBatch, gameTime);
            if (MenuState.IsShowLab2)
                lab2.Draw(spriteBatch, gameTime);
            if (MenuState.IsShowLab3)
                lab3.Draw(spriteBatch, gameTime);
            if (MenuState.IsShowLab4)
                lab4.Draw(spriteBatch, gameTime);

            if (MenuState.Prison)
                _prison.Draw(spriteBatch, gameTime);

            if (MenuState.Forest_1)
                forest_1.Draw(spriteBatch, gameTime);
            if (MenuState.Forest_2)
                forest_2.Draw(spriteBatch, gameTime);
           



            base.Draw(gameTime);
        }
    }
}
