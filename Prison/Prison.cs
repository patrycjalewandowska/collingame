﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Collin;
using Collin.Sprites;

namespace Collin_Game.Levels
{
    class Prison : DrawableGameComponent
    {
        // private MainGame mainGame;
        private Game game;
        private float _timer;

        public bool fall_bomb = false;

        Texture2D Player;
        Vector2 SpritePos;

        Texture2D Bomb, bomb1, bomb2, bomb3, bomb4;
        Vector2 BombPos, bomb1pos, bomb2pos, bomb3pos, bomb4pos;
        Rectangle recBomb;
       
        private List<Texture2D> _sprites;

        Texture2D Room_2;
        Vector2 posRoom_2;
        Rectangle recRoom_2;


        Texture2D healthBar;
        Vector2 pos_healthBar;
        Rectangle rec_healthBar;



      
        public static Random Random;
        public static int ScreenWidth;
        public static int ScreenHeight;

        public Prison(Game game) : base(game)
        {this.game = game;
            LoadContent(); }

        public override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            //create new objects

           

            //ScreenWidth = graphics.PreferredBackBufferWidth;
            //ScreenHeight = graphics.PreferredBackBufferHeight;

        }
        protected override void LoadContent()
        {
            Player = game.Content.Load<Texture2D>("collin2");
            Room_2 = game.Content.Load<Texture2D>("prison_1");
            Bomb = game.Content.Load<Texture2D>("Bomb");
            bomb1 = game.Content.Load<Texture2D>("Bomb"); ;
            bomb2 =  game.Content.Load<Texture2D>("Bomb");
            bomb3 = game.Content.Load<Texture2D>("Bomb"); ;
            bomb4 = game.Content.Load<Texture2D>("Bomb");


            _sprites = new List<Texture2D>()
        {   Bomb, bomb4,bomb3,bomb2,bomb1
            }; SpritePos = new Vector2(250, 400);

           recRoom_2.Height = 500;
            recRoom_2.Width = 625;


            healthBar = game.Content.Load<Texture2D>("Pasek_zycia");

            pos_healthBar = new Vector2(10, 450);
            // healthBar.Width = 150
            // healthBar.Height = 20
            rec_healthBar = new Rectangle(0, 0, 150, 15);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            spriteBatch.Draw(Room_2, recRoom_2, Color.White);

            spriteBatch.Draw(Bomb, BombPos,  Color.White);
            
            spriteBatch.Draw(bomb1, bomb1pos, Color.White);
            spriteBatch.Draw(bomb2, bomb2pos, Color.White);
            spriteBatch.Draw(bomb3,bomb3pos , Color.White);
            spriteBatch.Draw(bomb4,bomb4pos , Color.White);


            spriteBatch.Draw(Player, SpritePos, Color.White);

            spriteBatch.Draw(healthBar, pos_healthBar, rec_healthBar, Color.White);
            spriteBatch.End();

        }
        private void MoveSprite()
        {
            KeyboardState currKey = Keyboard.GetState();
            if (currKey.IsKeyDown(Keys.Left)) SpritePos.X -= 1;
            if (currKey.IsKeyDown(Keys.Right)) SpritePos.X += 1;
            if (currKey.IsKeyDown(Keys.Up)) SpritePos.Y -= 1;
            if (currKey.IsKeyDown(Keys.Down)) SpritePos.Y += 1;

        }
        private void Bombs() { 
        //{
        //    for(int i=0; i<=4;i++)
        //    {
        //       // _sprites[i];
        //        Random = new Random();
        //        float x = Random.Next(30, 500);

        //        fall_bomb = true;

        //        BombPos = new Vector2(x, 0);

  //  }
            if (fall_bomb == false)
            {
                Random = new Random();
                float x = Random.Next(30, 130);
                float a = Random.Next(130, 230); 
                float b = Random.Next(230, 330);
                float c = Random.Next(330, 430);
                float d = Random.Next(430, 500);


                fall_bomb = true;

                BombPos = new Vector2(x, 0);
                bomb1pos = new Vector2(a, 0);
                bomb2pos  =new Vector2(b, 0);
                bomb3pos = new Vector2(c, 0);
                bomb4pos = new Vector2(d, 0);


            }



            if (fall_bomb == true) BombPos.Y += 3; bomb3pos.Y += 3; bomb1pos.Y += 3; bomb2pos.Y += 3; bomb4pos.Y += 3;
            

            if (BombPos.Y >= 400)
            {
                fall_bomb = false;
            }

            // uderzenie bomby 

            if ((SpritePos.X == BombPos.X && SpritePos.Y == BombPos.Y) || (SpritePos.X <= BombPos.X + 70 && SpritePos.X >= BombPos.X - 10 && SpritePos.Y <= BombPos.Y + 30 && SpritePos.Y >= BombPos.Y - 10)) MenuState.IsShowGameOverScene = true;
            if ((SpritePos.X == bomb1pos.X && SpritePos.Y == bomb1pos.Y) || (SpritePos.X <= bomb1pos.X + 70 && SpritePos.X >= bomb1pos.X - 10 && SpritePos.Y <= bomb1pos.Y + 30 && SpritePos.Y >= bomb1pos.Y - 10)) MenuState.IsShowGameOverScene = true;
            if ((SpritePos.X == bomb2pos.X && SpritePos.Y == bomb2pos.Y) || (SpritePos.X <= bomb2pos.X + 70 && SpritePos.X >= bomb2pos.X - 10 && SpritePos.Y <= bomb2pos.Y + 30 && SpritePos.Y >= bomb2pos.Y - 10)) MenuState.IsShowGameOverScene = true;
            if ((SpritePos.X == bomb3pos.X && SpritePos.Y == bomb3pos.Y) || (SpritePos.X <= bomb3pos.X + 70 && SpritePos.X >= bomb3pos.X - 10 && SpritePos.Y <= bomb3pos.Y + 30 && SpritePos.Y >= bomb3pos.Y - 10)) MenuState.IsShowGameOverScene = true;
            if ((SpritePos.X == bomb4pos.X && SpritePos.Y == bomb4pos.Y) || (SpritePos.X <= bomb4pos.X + 70 && SpritePos.X >= bomb4pos.X - 10 && SpritePos.Y <= bomb4pos.Y + 30 && SpritePos.Y >= bomb4pos.Y - 10)) MenuState.IsShowGameOverScene = true;




        }
        private void LimitsWalls()
        {
            if (SpritePos.X <= 40) SpritePos.X = 40;
            if (SpritePos.Y >= 420) SpritePos.Y = 420;
            if (SpritePos.Y <= 120) SpritePos.Y = 120;
            if (SpritePos.X >= 540) SpritePos.X = 540;
        }
        private void Health()
        {
            // PASEK ZYCIA
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                rec_healthBar.Width -= 1;

            }
            else
            {
                rec_healthBar.Width += 1;
                if (rec_healthBar.Width >= 150)
                {
                    rec_healthBar.Width = 150;
                }

                if (rec_healthBar.Width <= 0)
                {
                    rec_healthBar.Width = 0;

                    if (rec_healthBar.Width == 0)
                    {
                        MenuState.IsShowGameOverScene = true;
                        rec_healthBar.Width = 150;
                        SpritePos = new Vector2(50, 50);
                    }

                }
            }
        }


        public void Update()
        {
            MoveSprite();
            Bombs();
            LimitsWalls();
            Health(); }
    }
}
